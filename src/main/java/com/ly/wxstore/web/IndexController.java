package com.ly.wxstore.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created with IntelliJ IDEA.
 * User: xvitcoder
 * Date: 12/20/12
 * Time: 5:27 PM
 */
@Controller
@RequestMapping("/")
public class IndexController {
	
	private static Logger logger = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping
    public String getIndexPage() {
    	logger.info("index");
        return "index";
    }
}
