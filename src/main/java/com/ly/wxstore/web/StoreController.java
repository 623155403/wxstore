/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.wxstore.web;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ly.wxstore.comm.Error;
import com.ly.wxstore.entity.goods.Goods;
import com.ly.wxstore.entity.goods.GoodsImages;
import com.ly.wxstore.exception.ApiErrorInfo;
import com.ly.wxstore.exception.ServerException;
import com.ly.wxstore.service.account.ShiroDbRealm.ShiroUser;
import com.ly.wxstore.service.goods.GoodsImagesService;
import com.ly.wxstore.service.goods.GoodsService;
import com.ly.wxstore.service.weixin.WeixinConf;

/**
 * 商城controller
 * 
 * @author peter
 */
@Controller
@RequestMapping(value = "/Store")
public class StoreController {

	private static Logger logger = LoggerFactory.getLogger(StoreController.class);

	@Autowired
	private GoodsService goodsService;

	@Autowired
	private GoodsImagesService goodsImagesService;

	@Autowired
	private WeixinConf weixinConf;

	/**
	 * 商品列表接口
	 * 
	 * @return
	 */
	@RequiresRoles("user")
	@RequestMapping(value = "goodslist.json", method = RequestMethod.GET)
	@ResponseBody
	public List<Goods> list() {
		logger.info("Store/list");
		List<Goods> list = null;
		try {
			list = goodsService.getByWeixinPublicId(weixinConf.getPublicId());
		} catch (ServerException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}

		return list;
	}

	/**
	 * 商品详情
	 * 
	 * @return
	 */
	@RequiresRoles("user")
	@RequestMapping(value = "goodsDetail/{goodsId}.json", method = RequestMethod.GET)
	@ResponseBody
	public Goods goodsDetail(@PathVariable("goodsId") Long goodsId) {
		logger.info("goodsDetail：" + goodsId);

		Goods goods = null;
		try {
			List<GoodsImages> images = goodsImagesService.getByGoodsId(goodsId);

			goods = goodsService.getById(goodsId);

			goods.setImages(images);
		} catch (ServerException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}

		return goods;
	}

	public String getCurrentUserSid() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user.getSid();
	}

	@ExceptionHandler(ServerException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ApiErrorInfo handleInvalidRequestError(ServerException ex) {
		return new ApiErrorInfo(ex.getCode(), ex.getMessage());
	}

}
