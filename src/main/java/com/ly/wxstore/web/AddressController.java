/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.wxstore.web;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ly.wxstore.comm.Error;
import com.ly.wxstore.entity.UserAddress;
import com.ly.wxstore.exception.ApiErrorInfo;
import com.ly.wxstore.exception.ServerException;
import com.ly.wxstore.service.UserAddressService;
import com.ly.wxstore.service.account.ShiroDbRealm.ShiroUser;

/**
 * 商城controller
 * 
 * @author peter
 */
@Controller
@RequestMapping(value = "/Address")
public class AddressController {

	private static Logger logger = LoggerFactory.getLogger(AddressController.class);

	@Autowired
	private UserAddressService addressService;

	/**
	 * 收货地址列表接口
	 * 
	 * @return
	 */
	@RequiresRoles("user")
	@RequestMapping(value = "addressList.json", method = RequestMethod.GET)
	@ResponseBody
	public List<UserAddress> list() {
		logger.info("Address/addressList");
		try {
			List<UserAddress> list = addressService.getAllBySid(getCurrentUserSid());
			return list;
		} catch (ServerException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}
	}

	/**
	 * 收货地址列表接口
	 * 
	 * @return
	 */
	@RequiresRoles("user")
	@RequestMapping(value = "defaultAddress.json", method = RequestMethod.GET)
	@ResponseBody
	public UserAddress defaultAddress() {
		logger.info("Address/defaultAddress");
		try {
			UserAddress address = addressService.getDefaultBySid(getCurrentUserSid());
			return address;
		} catch (ServerException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}
	}
	
	
	/**
	 * 收货地址列表接口
	 * 
	 * @return
	 */
	@RequiresRoles("user")
	@RequestMapping(value = "save.json", method = RequestMethod.POST)
	@ResponseBody
	public UserAddress saveAddress(@RequestBody UserAddress addr) {
		logger.info("Address/saveAddress");
		try {
			addr.setSid(getCurrentUserSid());
			addr.setDefaulted(0L);
			addressService.createOrUpdate(addr);
			return addr;
		} catch (ServerException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}
	}
	
	/**
	 * 删除收货地址
	 * 
	 * @return
	 */
	@RequiresRoles("user")
	@RequestMapping(value = "delete/{addId}.json", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@PathVariable("addId") Long addId) {
		logger.info("Address/saveAddress");
		try {
			addressService.delete(addId);
		} catch (ServerException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}
	}

	public String getCurrentUserSid() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user.getSid();
	}

	@ExceptionHandler(ServerException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ApiErrorInfo handleInvalidRequestError(ServerException ex) {
		return new ApiErrorInfo(ex.getCode(), ex.getMessage());
	}

}
