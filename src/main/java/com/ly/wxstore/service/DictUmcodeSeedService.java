package com.ly.wxstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ly.wxstore.entity.DictUmcodeSeed;
import com.ly.wxstore.repository.DictUmcodeSeedDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class DictUmcodeSeedService {

	@Autowired
	private DictUmcodeSeedDao umcodeSeedDao;

	public synchronized String getUmcode(Long weixinPublicId) {
		DictUmcodeSeed umcodeSeed = getByWeixinPublicId(weixinPublicId);
		String umcodeSeedStr = umcodeSeed.getUmcodeSeed();
		Long umcode = Long.valueOf(umcodeSeedStr)+1;
		umcodeSeedDao.updateUmcodeSeed(umcodeSeed.getId(),umcode.toString());
		return umcode.toString();
	}

	@Transactional
	private DictUmcodeSeed getByWeixinPublicId(Long weixinPublicId) {
		DictUmcodeSeed umcodeSeed = umcodeSeedDao.getByWeixinPublicId(weixinPublicId);

		return umcodeSeed;
	}

}
