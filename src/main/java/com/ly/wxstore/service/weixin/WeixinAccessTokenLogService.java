package com.ly.wxstore.service.weixin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.weixin.WeixinAccessTokenLog;
import com.ly.wxstore.repository.weixin.WeixinAccessTokenLogDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinAccessTokenLogService {

	@Autowired
	private WeixinAccessTokenLogDao weixinAccessTokenLogDao;

	public WeixinAccessTokenLog getById(Long id) {
		return weixinAccessTokenLogDao.getById(id);
	}

	public List<WeixinAccessTokenLog> getAll() {
		return weixinAccessTokenLogDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinAccessTokenLog> searchPage(WeixinAccessTokenLog weixinAccessTokenLog, int currentPage, int pageSize) {
		MyPage<WeixinAccessTokenLog> myPage = new MyPage<WeixinAccessTokenLog>();

		Long count = weixinAccessTokenLogDao.searchCount(weixinAccessTokenLog);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinAccessTokenLog> list = weixinAccessTokenLogDao.searchPage(weixinAccessTokenLog, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinAccessTokenLog weixinAccessTokenLog) {
		weixinAccessTokenLogDao.save(weixinAccessTokenLog);
	}

	public void update(WeixinAccessTokenLog weixinAccessTokenLog) {
		weixinAccessTokenLogDao.update(weixinAccessTokenLog);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinAccessTokenLogDao.delete(id);
	}
}
