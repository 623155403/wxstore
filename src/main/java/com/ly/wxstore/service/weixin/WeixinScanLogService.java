package com.ly.wxstore.service.weixin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.weixin.WeixinScanLog;
import com.ly.wxstore.repository.weixin.WeixinScanLogDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinScanLogService {

	@Autowired
	private WeixinScanLogDao weixinScanLogDao;

	public WeixinScanLog getById(Long id) {
		return weixinScanLogDao.getById(id);
	}

	public List<WeixinScanLog> getAll() {
		return weixinScanLogDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinScanLog> searchPage(WeixinScanLog weixinScanLog, int currentPage, int pageSize) {
		MyPage<WeixinScanLog> myPage = new MyPage<WeixinScanLog>();

		Long count = weixinScanLogDao.searchCount(weixinScanLog);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinScanLog> list = weixinScanLogDao.searchPage(weixinScanLog, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinScanLog weixinScanLog) {
		weixinScanLogDao.save(weixinScanLog);
	}

	public void update(WeixinScanLog weixinScanLog) {
		weixinScanLogDao.update(weixinScanLog);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinScanLogDao.delete(id);
	}
}
