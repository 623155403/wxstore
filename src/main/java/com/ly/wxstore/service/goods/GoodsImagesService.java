package com.ly.wxstore.service.goods;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.goods.GoodsImages;
import com.ly.wxstore.repository.weixin.GoodsImagesDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class GoodsImagesService {

	private static Logger logger = LoggerFactory.getLogger(GoodsImagesService.class);

	@Autowired
	private GoodsImagesDao goodsImagesDao;

	public GoodsImages getById(Long id) {
		return goodsImagesDao.getById(id);
	}

	public List<GoodsImages> getAll() {
		return goodsImagesDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<GoodsImages> searchPage(GoodsImages goodsImages, int currentPage, int pageSize) {
		MyPage<GoodsImages> myPage = new MyPage<GoodsImages>();

		Long count = goodsImagesDao.searchCount(goodsImages);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<GoodsImages> list = goodsImagesDao.searchPage(goodsImages, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(GoodsImages goodsImages) {
		goodsImagesDao.save(goodsImages);
	}

	public void update(GoodsImages goodsImages) {
		goodsImagesDao.update(goodsImages);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		goodsImagesDao.delete(id);
	}

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public List<GoodsImages> getByGoodsId(Long goodsId) {
		return goodsImagesDao.getByGoodsId(goodsId);
	}
}
