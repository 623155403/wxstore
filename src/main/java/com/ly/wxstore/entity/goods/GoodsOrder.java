package com.ly.wxstore.entity.goods;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ly.wxstore.comm.OrderStatus;

public class GoodsOrder {

	public GoodsOrder() {
	}

	private Long id; //
	private Date createDate; // 创建时间
	private Date updateDate; // 最后更新时间
	private String updateSid; // 最后更新人sid
	private String createSid; // 创建人的场景值sid
	private Integer deleted; // 是否删除（0：删，1：用）
	private Long weixinPublicId; // 微信公众号id
	private Long orderStatus; // 订单状态(1.代付款，2.待发货，3.待收货，4.待评价)
	private Double money; // 订单金额
	private Double realMoney; // 实付金额
	private String orderCode; // 订单编号
	private String openid; //

	private Double payMoney; // 支付金额
	private String payCode; // 支付编号
	private Long payStatus; // 支付状态: 0：未支付；1：支付成功；2：支付失败

	public Long getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Long payStatus) {
		this.payStatus = payStatus;
	}

	/**
	 * 支付金额
	 * 
	 * @return
	 */
	public Double getPayMoney() {
		return payMoney;
	}

	/**
	 * 支付金额
	 * 
	 * @param payMoney
	 */
	public void setPayMoney(Double payMoney) {
		this.payMoney = payMoney;
	}

	/**
	 * 支付编号
	 * 
	 * @return
	 */
	public String getPayCode() {
		return payCode;
	}

	/**
	 * 支付编号
	 * 
	 * @param payCode
	 */
	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	/**
     *
     **/
	public Long getId() {
		return id;
	}

	/**
	 *
	 **/
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 创建时间
	 **/
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * 创建时间
	 **/
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 最后更新时间
	 **/
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * 最后更新时间
	 **/
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * 最后更新人sid
	 **/
	public String getUpdateSid() {
		return updateSid;
	}

	/**
	 * 最后更新人sid
	 **/
	public void setUpdateSid(String updateSid) {
		this.updateSid = updateSid;
	}

	/**
	 * 创建人的场景值sid
	 **/
	public String getCreateSid() {
		return createSid;
	}

	/**
	 * 创建人的场景值sid
	 **/
	public void setCreateSid(String createSid) {
		this.createSid = createSid;
	}

	/**
	 * 是否删除（0：删，1：用）
	 **/
	public Integer getDeleted() {
		return deleted;
	}

	/**
	 * 是否删除（0：删，1：用）
	 **/
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	/**
	 * 微信公众号id
	 **/
	public Long getWeixinPublicId() {
		return weixinPublicId;
	}

	/**
	 * 微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId) {
		this.weixinPublicId = weixinPublicId;
	}

	/**
	 * 订单状态(1.代付款，2.待发货，3.待收货，4.待评价)
	 **/
	public Long getOrderStatus() {
		return orderStatus;
	}

	/**
	 * 订单状态(1.代付款，2.待发货，3.待收货，4.待评价)
	 **/
	public void setOrderStatus(Long orderStatus) {
		this.orderStatus = orderStatus;
	}

	/**
	 * 订单金额（根据订单商品列表计算）
	 **/
	public Double getMoney() {
		this.money = 0.0;
		List<GoodsOrderDetail> goodsList = this.getGoodsList();
		if (goodsList != null && goodsList.size() > 0) {
			for (GoodsOrderDetail goodsOrderDetail : goodsList) {
				this.money += (goodsOrderDetail.getPrice() * goodsOrderDetail.getGoodsNumber());
			}
		}
		return this.money;
	}

	/**
	 * 订单金额
	 **/
	public void setMoney(Double money) {
		this.money = money;
	}

	/**
	 * 实付金额（根据订单商品列表计算）
	 **/
	public Double getRealMoney() {
		this.realMoney = 0.0;
		List<GoodsOrderDetail> goodsList = this.getGoodsList();
		if (goodsList != null && goodsList.size() > 0) {
			for (GoodsOrderDetail goodsOrderDetail : goodsList) {
				this.realMoney += (goodsOrderDetail.getRealPrice() * goodsOrderDetail.getGoodsNumber());
			}
		}
		return this.realMoney;
	}

	/**
	 * 实付金额
	 **/
	public void setRealMoney(Double realMoney) {
		this.realMoney = realMoney;
	}

	/**
	 * 订单编号
	 **/
	public String getOrderCode() {
		return orderCode;
	}

	/**
	 * 订单编号
	 **/
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	/**
     *
     **/
	public String getOpenid() {
		return openid;
	}

	/**
	 *
	 **/
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	private String nickname; // 微信昵称

	private List<GoodsOrderDetail> goodsList = new ArrayList<GoodsOrderDetail>();// 订单商品列表

	private GoodsDeliveryAddress goodsDeliveryAddress;// 收货地址

	public List<GoodsOrderDetail> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<GoodsOrderDetail> goodsList) {
		this.goodsList = goodsList;
	}

	public GoodsDeliveryAddress getGoodsDeliveryAddress() {
		return goodsDeliveryAddress;
	}

	public void setGoodsDeliveryAddress(GoodsDeliveryAddress goodsDeliveryAddress) {
		this.goodsDeliveryAddress = goodsDeliveryAddress;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getOrderStatusName() {
		return OrderStatus.getOrderStatusName(orderStatus == null ? "1" : orderStatus.toString());
	}

	public void setOrderStatusName(String orderStatusName) {

	}

	private String clientIp;

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	/**
	 * 交易结束时间 订单创建时间+30天 注意：如果没有对应的Setter方法，则在前端ajax方法直接返回对象的时候会报500错误，因此需要用@JsonIgnore
	 * 
	 * @return
	 */
	@JsonIgnore
	public Date getExpireDate() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.getCreateDate());
		cal.add(Calendar.DAY_OF_MONTH, 30);
		return cal.getTime();
	}

}