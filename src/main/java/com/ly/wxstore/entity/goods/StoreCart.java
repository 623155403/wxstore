package com.ly.wxstore.entity.goods;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class StoreCart {
	
	/**
	 * 该商品已下单，不在购物车中显示
	 */
	public static final Long TO_ORDER_YES = 1l;
	
	/**
	 * 该商品未下单，在购物车显示
	 */
	public static final Long TO_ORDER_NO = 0l;

	public StoreCart() {
	}

	private Long id; //
	private Long goodsId; // 商品id
	
	private Long goodsNumber; // 购买数量
	private String openid; //
	private Long deleted; //是否删除（0：删；1：用）
	private Date createDate; // 创建时间
	private Date updateDate; // 最后更新时间
	private String updateSid; // 最后更新人sid
	private String createSid; // 创建人的场景值sid
	private Long weixinPublicId; // 微信公众号id
	private Long toOrder; //是否下单0：否；1：已下单
	
	private Double costPrice; // 进货价格
	private Double price; // 销售价格（折前价格）
	private Double realPrice; // 实际销售价格（折后价格）
	private String goodsDesc; // 商品描述
	private String goodsName; // 商品名称
	private String goodsStatus; // 商品状态，1上架，2下架
	private String goodsImageUrl; //
	
	public String getGoodsStatus() {
		return goodsStatus;
	}

	public void setGoodsStatus(String goodsStatus) {
		this.goodsStatus = goodsStatus;
	}

	public Long getToOrder() {
		return toOrder;
	}

	public void setToOrder(Long toOrder) {
		this.toOrder = toOrder;
	}

	public String getGoodsImageUrl() {
		return goodsImageUrl;
	}

	public void setGoodsImageUrl(String goodsImageUrl) {
		this.goodsImageUrl = goodsImageUrl;
	}

	/**
     *
     **/
	public Long getId() {
		return id;
	}

	/**
	 *
	 **/
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 商品id
	 **/
	public Long getGoodsId() {
		return goodsId;
	}

	/**
	 * 商品id
	 **/
	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	/**
	 * 进货价格
	 **/
	public Double getCostPrice() {
		return costPrice;
	}

	/**
	 * 进货价格
	 **/
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	/**
	 * 销售价格（折前价格）
	 **/
	public Double getPrice() {
		return price;
	}

	/**
	 * 销售价格（折前价格）
	 **/
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * 实际销售价格（折后价格）
	 **/
	public Double getRealPrice() {
		return realPrice;
	}

	/**
	 * 实际销售价格（折后价格）
	 **/
	public void setRealPrice(Double realPrice) {
		this.realPrice = realPrice;
	}

	/**
	 * 商品描述
	 **/
	public String getGoodsDesc() {
		return goodsDesc;
	}

	/**
	 * 商品描述
	 **/
	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}

	/**
	 * 商品名称
	 **/
	public String getGoodsName() {
		return goodsName;
	}

	/**
	 * 商品名称
	 **/
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	/**
	 * 购买数量
	 **/
	public Long getGoodsNumber() {
		return goodsNumber;
	}

	/**
	 * 购买数量
	 **/
	public void setGoodsNumber(Long goodsNumber) {
		this.goodsNumber = goodsNumber;
	}

	/**
     *
     **/
	public String getOpenid() {
		return openid;
	}

	/**
	 *
	 **/
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
     *
     **/
	public Long getDeleted() {
		return deleted;
	}

	/**
	 *
	 **/
	public void setDeleted(Long deleted) {
		this.deleted = deleted;
	}

	/**
	 * 创建时间
	 **/
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * 创建时间
	 **/
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 最后更新时间
	 **/
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * 最后更新时间
	 **/
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * 最后更新人sid
	 **/
	public String getUpdateSid() {
		return updateSid;
	}

	/**
	 * 最后更新人sid
	 **/
	public void setUpdateSid(String updateSid) {
		this.updateSid = updateSid;
	}

	/**
	 * 创建人的场景值sid
	 **/
	public String getCreateSid() {
		return createSid;
	}

	/**
	 * 创建人的场景值sid
	 **/
	public void setCreateSid(String createSid) {
		this.createSid = createSid;
	}

	/**
	 * 微信公众号id
	 **/
	public Long getWeixinPublicId() {
		return weixinPublicId;
	}

	/**
	 * 微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId) {
		this.weixinPublicId = weixinPublicId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	private boolean selected;// 是否选中

	public boolean getSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}