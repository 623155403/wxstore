package com.ly.wxstore.repository.fin;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.fin.FinBrokerageStatistics;
import com.ly.wxstore.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface FinBrokerageStatisticsDao {
	
	FinBrokerageStatistics getById(Long id);
	
	List<FinBrokerageStatistics> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<FinBrokerageStatistics> searchPage(@Param("finBrokerageStatistics")FinBrokerageStatistics finBrokerageStatistics,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(FinBrokerageStatistics finBrokerageStatistics);
	
	void save(FinBrokerageStatistics finBrokerageStatistics);
	
	void update(FinBrokerageStatistics finBrokerageStatistics);
	
	/**
	 * 软删除
	 */
	void delete(Long id);

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++
	FinBrokerageStatistics getBySid(String sid);
	

}
