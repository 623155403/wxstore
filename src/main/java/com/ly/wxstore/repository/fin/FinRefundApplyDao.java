package com.ly.wxstore.repository.fin;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.fin.FinRefundApply;
import com.ly.wxstore.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface FinRefundApplyDao {
	
	FinRefundApply getById(Long id);
	
	List<FinRefundApply> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<FinRefundApply> searchPage(@Param("finRefundApply")FinRefundApply finRefundApply,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(FinRefundApply finRefundApply);
	
	void save(FinRefundApply finRefundApply);
	
	void update(FinRefundApply finRefundApply);
	
	/**
	 * 软删除
	 */
	void delete(Long id);
	

}
