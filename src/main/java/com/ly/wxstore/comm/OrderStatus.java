package com.ly.wxstore.comm;

import java.util.HashMap;
import java.util.Map;

/**
 * 订单状态
 * 
 * @author Peter
 *
 */
public class OrderStatus {

	private static Map<String, String> status;
	static {
		status = new HashMap<String, String>();
		status.put("1", "待付款");// 交易中
		status.put("2", "待发货");// 交易中
		status.put("3", "待收货");// 交易中
		status.put("4", "确认收货");// 交易成功、待评价
		status.put("5", "取消");// 交易关闭(代付款状态可以取消订单)
		status.put("6", "退款");// 交易中（付款后--确认收货前可以申请退款[2,3]）
		status.put("7", "退款成功");// 交易关闭
		status.put("8", "取消退款");// 交易成功
	}

	public static String getOrderStatusName(String code) {
		return status.get(code);
	}

	/**
	 * 未付款（交易中）
	 */
	public static final Long UNPAY = 1L;

	/**
	 * 待发货（交易中）
	 */
	public static final Long WAITING_DELIVER = 2L;

	/**
	 * 待收货（交易中）
	 */
	public static final Long WAITING_RECEIVING = 3L;

	/**
	 * 确认收货、待评价（交易成功）
	 */
	public static final Long WAITING_FEEDBACK = 4L;

	/**
	 * 取消（交易关闭）
	 */
	public static final Long CANCEL = 5L;

	/**
	 * 退款 （交易中）（付款后--确认收货前可以申请退款[2,3]）
	 */
	public static final Long REFUND = 6L;

	/**
	 * 退款成功（交易关闭）
	 */
	public static final Long REFUND_SUCCESS = 7L;

	/**
	 * 取消退款（交易成功）
	 */
	public static final Long REFUND_CANCEL = 8L;

}
