package com.ly.wxstore.comm;

/**
 * 销售角色
 * 销售角色：fans（粉丝）、broker(经纪人)、partner(合伙人)
 * @author Peter
 *
 */
public class SalerRole {
	
	/**
	 * fans（粉丝：没有成功交易记录、仅仅关注公众号的用户）
	 */
	public static final String fans="fans";
	/**
	 * broker(经纪人：有过成功交易的微信用户)
	 */
	public static final String broker="broker";
	/**
	 * partner(合伙人：业务比较复杂，需要专业销售签订合同等，合伙人下面的经纪人不分等级)
	 */
	public static final String partner="partner";

}
