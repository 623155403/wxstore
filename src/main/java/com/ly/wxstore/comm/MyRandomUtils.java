package com.ly.wxstore.comm;

import java.util.UUID;

public class MyRandomUtils {
	
	/**
	 * 32 位随机数，并且是大写
	 * @return
	 */
	public static String getRandom32StringUpperCase(){
		String str = UUID.randomUUID().toString();
		str = str.replace("-", "");
		
		
		return str.toUpperCase();
	}
	
	/**
	 * 32 位随机数
	 * @return
	 */
	public static String getRandom32String(){
		String str = UUID.randomUUID().toString();
		str = str.replace("-", "");
		
		
		return str;
	}

	public static void main(String[] args) {
		System.out.println(getRandom32StringUpperCase());
	}
}
