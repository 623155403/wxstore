package com.ly.wxstore.comm;

/**
 * 销售级别
 * @author Peter
 *
 */
public class SalerLevel {
	
	public static final Long LEVEL_1 = 1L;
	public static final Long LEVEL_2 = 2L;
	public static final Long LEVEL_3 = 3L;
}
