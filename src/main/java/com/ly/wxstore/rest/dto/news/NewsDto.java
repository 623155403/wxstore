package com.ly.wxstore.rest.dto.news;

import java.util.List;

public class NewsDto {

	private List<Item> item;

	private String total_count;

	private String item_count;

	public void setItem(List<Item> item) {
		this.item = item;
	}

	public List<Item> getItem() {
		return this.item;
	}

	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}

	public String getTotal_count() {
		return this.total_count;
	}

	public void setItem_count(String item_count) {
		this.item_count = item_count;
	}

	public String getItem_count() {
		return this.item_count;
	}
}
