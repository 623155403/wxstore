'use strict';

/**
 * 商城
 * GoodsDetailController
 * @constructor
 */
WxStoreApp.controller('StoreGoodsDetailController', ["$rootScope","$scope","$http","$stateParams","$ionicPopup", "$location", "$state",
  function($rootScope, $scope, $http, $stateParams, $ionicPopup, $location, $state) {
	
	$scope.fetchGoodsDetail = function() {
        $http.get('Store/goodsDetail/' + $stateParams.goodsId + '.json').success(function(goodsDetail){
            $scope.goodsDetail = goodsDetail;
            console.log(goodsDetail);
        }).error(function(data, status, headers, config) {
        	$rootScope.wxstore.hideLoading();
        	console.log(data+"-"+status+"-"+headers+"-"+config);
        	$rootScope.wxstore.showError(data);
        });
    };
    
    $scope.fetchGoodsDetail();
	
    //下拉刷新页面
    $scope.doRefresh = function() {
    	 $http.get('Store/goodsDetail/' + $stateParams.goodsId + '.json').success(function(goodsDetail){
             $scope.goodsDetail = goodsDetail;
             console.log(goodsDetail);
         }).error(function(data, status, headers, config) {
        	 $rootScope.wxstore.showError(data);
         }).finally(function() {
        	 $scope.$broadcast('scroll.refreshComplete');
         });
     };
      
     //添加到购物车
	 $scope.addToCard = function() {
	     $http.get('Cart/addToCart/' + $scope.goodsDetail.id + '.json').success(function(isSuccess){
	    	 $rootScope.wxstore.badgeData.cartBadgeCount = $rootScope.wxstore.badgeData.cartBadgeCount+1;
	    	 //$scope.show("亲，我在购物车等您哦！...");
	     }).error(function(data, status, headers, config) {
        	console.log(data+"-"+status+"-"+headers+"-"+config);
        	$rootScope.wxstore.showError(data);
        });
	 };
	 
	 //立即购买
	 $scope.buy = function(goodsId){
		 var orderVoList = new Array();  
		 orderVoList.push({"goodsId":goodsId,"goodsNumber":1});
		 $rootScope.wxstore.orderVoListParam = orderVoList;//用户选中的商品
		 $rootScope.wxstore.orderFrom = "store";
		 $state.go('tabs.storeOrderConfirm');
	 }
	 
}]);


