'use strict';

/**
 * 个人中心
 * MyCenterController
 * @constructor
 */
WxStoreApp.controller('MyCenterController', ["$scope","$http","$ionicModal",
	function($scope, $http, $ionicModal) {
	console.log('HomeTabCtrl');
	
    $scope.fetchCarsList = function() {
        $http.get('MyCenter/userInfolist.json').success(function(myCenterPageVo){
        	 console.log(myCenterPageVo);
             $scope.myCenterPageVo = myCenterPageVo;
        }).error(function(data, status, headers, config) {
        	 $rootScope.wxstore.showError(data);
        });
    };
    $scope.fetchCarsList();

}]);