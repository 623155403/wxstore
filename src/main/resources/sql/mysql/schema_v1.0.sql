drop table if exists t_marketing_account;

drop table if exists wx_weixin_access_token;

drop table if exists wx_weixin_access_token_log;

drop table if exists wx_weixin_event_push;

drop table if exists wx_weixin_event_push_article;

drop table if exists wx_weixin_jsapi_ticket;

drop table if exists wx_weixin_jsapi_ticket_log;

drop table if exists wx_weixin_public;

drop table if exists wx_weixin_scan_log;

drop table if exists wx_weixin_two_dimensional_code;

drop table if exists wx_weixin_user_info;

/*==============================================================*/
/* Table: t_marketing_account                                   */
/*==============================================================*/
create table t_marketing_account
(
   id                   bigint not null,
   openid               varchar(512),
   umcode               varchar(64) comment '用户营销号编UserMarkingCode(umcode)',
   scene_str            varchar(64),
   parent_scene_str     varchar(64),
   weixin_public_id     bigint,
   create_date          datetime comment '创建时间',
   primary key (id)
);

/*==============================================================*/
/* Table: wx_weixin_access_token                                */
/*==============================================================*/
create table wx_weixin_access_token
(
   id                   bigint not null auto_increment,
   access_token         varchar(512) comment '获取到的凭证',
   expires_in           bigint comment '凭证有效时间，单位：秒',
   create_date          datetime,
   update_date          datetime,
   weixin_public_id     bigint comment '微信公众号id',
   primary key (id)
);

alter table wx_weixin_access_token comment '微信Access_Token（非Oauth中获取用户信息的Access_Token）';

/*==============================================================*/
/* Table: wx_weixin_access_token_log                            */
/*==============================================================*/
create table wx_weixin_access_token_log
(
   id                   bigint not null auto_increment,
   access_token         varchar(512) comment '获取到的凭证',
   expires_in           bigint comment '凭证有效时间，单位：秒',
   create_date          datetime,
   errcode              varchar(512),
   errmsg               varchar(512),
   weixin_public_id     bigint comment '微信公众号id',
   primary key (id)
);

alter table wx_weixin_access_token_log comment '微信Access_Token（非Oauth中获取用户信息的Access_Token）';

/*==============================================================*/
/* Table: wx_weixin_event_push                                  */
/*==============================================================*/
create table wx_weixin_event_push
(
   id                   bigint not null auto_increment,
   create_date          datetime,
   update_date          datetime,
   event_type           varchar(64),
   event_code           varchar(64),
   event_name           varchar(255),
   event_key            varchar(255),
   event_desc           varchar(512),
   weixin_public_id     bigint comment '微信公众号id',
   primary key (id)
);

alter table wx_weixin_event_push comment '需要推送消息的事件';

/*==============================================================*/
/* Table: wx_weixin_event_push_article                          */
/*==============================================================*/
create table wx_weixin_event_push_article
(
   id                   bigint not null auto_increment,
   create_date          datetime,
   update_date          datetime,
   push_event_code      varchar(64) comment '事件类型：click菜单点击事件，text图文消息指令事件（比如用户回复“抽奖”，将会推送抽奖页面）',
   description          varchar(1024),
   title                varchar(512),
   pic_url              varchar(1024),
   url                  varchar(1024),
   seq                  bigint,
   weixin_public_id     bigint comment '微信公众号id',
   primary key (id)
);

alter table wx_weixin_event_push_article comment '根据不同事件推送的微信图文消息';

/*==============================================================*/
/* Table: wx_weixin_jsapi_ticket                                */
/*==============================================================*/
create table wx_weixin_jsapi_ticket
(
   id                   bigint not null auto_increment,
   jsapi_ticket         varchar(512) comment '获取到的凭证',
   expires_in           bigint comment '凭证有效时间，单位：秒',
   create_date          datetime,
   update_date          datetime,
   weixin_public_id     bigint comment '微信公众号id',
   primary key (id)
);

alter table wx_weixin_jsapi_ticket comment 'jsapi_ticket是公众号用于调用微信JS接口的临时票据。正常情况下，jsapi_ticket的有效期为7200秒';

/*==============================================================*/
/* Table: wx_weixin_jsapi_ticket_log                            */
/*==============================================================*/
create table wx_weixin_jsapi_ticket_log
(
   id                   bigint not null auto_increment,
   jsapi_ticket         varchar(512) comment '获取到的凭证',
   expires_in           bigint comment '凭证有效时间，单位：秒',
   create_date          datetime,
   errcode              varchar(512),
   errmsg               varchar(512),
   weixin_public_id     bigint comment '微信公众号id',
   primary key (id)
);

alter table wx_weixin_jsapi_ticket_log comment '微信jsapi_ticket获取日志，方便定位ticket获取中出现的问题';

/*==============================================================*/
/* Table: wx_weixin_public                                      */
/*==============================================================*/
create table wx_weixin_public
(
   id                   bigint not null auto_increment,
   create_date          datetime comment '创建时间',
   update_date          datetime comment '最后更新时间',
   create_user_uid      varchar(128) comment '创建人uid',
   update_user_uid      varchar(128) comment '最后更新人uid',
   deleted              int comment '是否删除（0：删，1：用）',
   marketing_id         bigint,
   app_id               varchar(128),
   app_secret           varchar(128),
   server_url           varchar(1024),
   token                varchar(128),
   encodingAESKey       varchar(128),
   redirect_uil         varchar(1024),
   public_mark_name     varchar(128),
   public_mark_password varchar(128),
   marketing_name       varchar(128),
   primary key (id)
);

alter table wx_weixin_public comment '微信公众号信息（营销号）';

/*==============================================================*/
/* Table: wx_weixin_scan_log                                    */
/*==============================================================*/
create table wx_weixin_scan_log
(
   id                   bigint not null auto_increment,
   create_date          datetime,
   update_date          datetime,
   openid               varchar(512),
   scene_str            varchar(64) comment '场景值',
   parent_scene_str     varchar(64) comment '父场景值',
   case_name            varchar(64) comment '扫码情景：（已关注扫码，取消关注扫码，新用户扫码）',
   case_code            varchar(64) comment '扫码情景code',
   current_parent_scene_str varchar(64),
   weixin_public_id     bigint comment '微信公众号id',
   primary key (id)
);

alter table wx_weixin_scan_log comment '微信带参数二维码，每个微信用户一个个人二维码';

/*==============================================================*/
/* Table: wx_weixin_two_dimensional_code                        */
/*==============================================================*/
create table wx_weixin_two_dimensional_code
(
   id                   bigint not null auto_increment,
   create_date          datetime,
   update_date          datetime,
   image_name           varchar(255),
   openid               varchar(512),
   scene_str            varchar(64),
   image_url            varchar(1024),
   weixin_public_id     bigint comment '微信公众号id',
   primary key (id)
);

alter table wx_weixin_two_dimensional_code comment '微信带参数二维码，每个微信用户一个个人二维码two_dimensional_code';

/*==============================================================*/
/* Table: wx_weixin_user_info                                   */
/*==============================================================*/
create table wx_weixin_user_info
(
   id                   bigint not null auto_increment,
   openid               varchar(512) comment '用户的唯一标识',
   nickname             varchar(64) comment '用户昵称',
   sex                  varchar(64) comment '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
   province             varchar(64) comment '用户个人资料填写的省份',
   city                 varchar(64) comment '普通用户个人资料填写的城市',
   country              varchar(64) comment '国家，如中国为CN',
   headimgurl           varchar(512) comment '用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。',
   privilege            varchar(512) comment '用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）',
   unionid              varchar(512) comment '只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。详见：获取用户个人信息（UnionID机制）',
   create_date          datetime,
   update_date          datetime,
   weixin_public_mark_id bigint comment '微信公众号id',
   primary key (id)
);

drop table if exists t_weixin_subscribe_log;

/*==============================================================*/
/* Table: t_weixin_subscribe_log                                */
/*==============================================================*/
create table t_weixin_subscribe_log
(
   id                   bigint not null auto_increment,
   create_date          datetime,
   openid               varchar(512),
   event                varchar(32) comment '事件类型，subscribe(订阅)、unsubscribe(取消订阅)',
   primary key (id)
);

alter table t_weixin_subscribe_log comment '微信带参数二维码，每个微信用户一个个人二维码';

drop table if exists t_umcode_seed;

/*==============================================================*/
/* Table: t_umcode_seed                                         */
/*==============================================================*/
create table t_umcode_seed
(
   id                   bigint not null auto_increment,
   create_date          datetime,
   weixin_public_id     bigint,
   umcode_seed          varchar(64),
   primary key (id)
);

alter table t_umcode_seed comment '每个用户针对一个微信号有一个ID，需要用户记住';
